# Ansible and YAML lint

### Usage in a .gitlab-ci.yml

```
---

stages:
  - lint

ansible:
  stage: lint
  needs: []
  image: registry.cri.epita.fr/cri/docker/ansible/master:latest
  script:
    - ansible-lint

yaml:
  stage: lint
  needs: []
  image: registry.cri.epita.fr/cri/docker/ansible/master:latest
  script:
    - yamllint -f colored
```
